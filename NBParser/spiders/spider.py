import scrapy

from NBParser.parser import PageParser


class NBParser(scrapy.Spider):
    name = "NBParser"
    start_urls = [
        # продажа
       'https://www.nb.by/kvartiry-prodazha-nedvizhimosti/?req=all', # Квартиры
       'https://www.nb.by/komnaty-prodazha-nedvizhimosti/', # Комнаты и доли
       'https://www.nb.by/doma-prodazha-nedvizhimosti/?req=all', # Дом, дача, коттедж
       'https://www.nb.by/uchastok-prodazha-nedvizhimosti/', # Участок
       'https://www.nb.by/garazhi-prodazha-nedvizhimosti/', # Гаражи, машиноместа
       'https://www.nb.by/comercheskaya-prodazha-nedvizhimosti/?req=all', # Коммерческая

        # аренда
        'https://www.nb.by/arenda-predlozhenie-kvartiry-sutki/', # Квартиры посуточно
        'https://www.nb.by/arenda-predlozhenie-doma-sutki/', # Коттеджи, агроусадьбы, дома посуточно
        'https://www.nb.by/arenda-predlozhenie-komnaty/', # Комнаты на длительный срок
        'https://www.nb.by/arenda-predlozhenie-kvartiry/', # Квартиры на длительный срок
        'https://www.nb.by/arenda-predlozhenie-doma/', # Коттеджи, дома, дачи на длительный срок
        'https://www.nb.by/arenda-predlozhenie-nezhilaja/', # Нежилые помещения: аренда складов, магазинов

    ]

    def parse(self, response):
        return PageParser.parse(response)
