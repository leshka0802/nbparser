
TITLE = 'title' # заголовок

IMAGES = 'images' # изображения
CATEGORY = 'category' # категория

PRICE_BYN = 'price_byn' # цена белор. руб.
PRICE_M2 = 'price_m2' # цена за кв. м.
CONTACT_TYPE = 'contact_type' # тип продавца
CONTACT_NAME = 'contact_name' # продавец
CONTACT_PHONE = 'contact_phone' # телефон

ROOM_COUNT = 'room_count' # количество комант
ROOM_FOR_SALE = 'room_for_sale' # количество комант на продажу
LEVEL = 'level' # этаж
CEILING_HEIGHT = 'ceiling_height' # высота потолков

AREA = 'area' # площадь всего
LAND_AREA = 'land_area' # участок
LIVING_AREA = 'living_area' # жилая площадь
KITCHEN_AREA = 'kitchen_area' # площадь кухни
ROOM_AREA = 'room_area' # площадь комнат

DESCRIPTION = 'description' # описание

CREATED = 'created' # дата добавления
UPDATED = 'updated' # дата обновления

DAY_VIEW = 'day_view' # просмотры за день
WEEK_VIEW = 'week_view' # просмотры за неделю
MONTH_VIEW = 'month_view' # просмотры за месяц

FURNISHING = 'furnishing' # оснащение

SITE_HOST = 'https://www.nb.by'


class PageParser:
    result = {
        TITLE: None,
        IMAGES: None,
        CATEGORY: None,
        PRICE_BYN: None,
        PRICE_M2: None,
        CONTACT_TYPE: None,
        CONTACT_NAME: None,
        CONTACT_PHONE: None,

        ROOM_COUNT: None,
        ROOM_FOR_SALE: None,
        LEVEL: None,
        CEILING_HEIGHT: None,

        AREA: None,
        LAND_AREA: None,
        LIVING_AREA: None,
        KITCHEN_AREA: None,
        ROOM_AREA: None,

        DESCRIPTION: None,

        CREATED: None,
        UPDATED: None,

        DAY_VIEW: None,
        WEEK_VIEW: None,
        MONTH_VIEW: None,

        FURNISHING: None,
    }

    @classmethod
    def parse(cls, response):
        """
        обход элементов на странице
        """
        # "item " пробел обязателен
        for item in response.xpath(
                '//div[@class="list_view"]/div[@class="item "]'):
            detail_url = item.xpath('./div/p/a/@href').extract_first()
            yield response.follow(detail_url, cls.detail_parse)

        next_page = response.xpath(
            '//div/ul/li/a[@id="next_page"]/@href').extract_first()
        if next_page is not None:
            yield response.follow(next_page, cls.parse)

    @classmethod
    def detail_parse(cls, response):
        """
        Получение инф. по объекту
        """
        catalog = response.xpath('//div[contains(@class, "catalog")]')
        result = cls.result.copy()

        result.update({
            TITLE: catalog.xpath('h1/text()').extract_first() or '',
            IMAGES: ', '.join(
                map(lambda x: SITE_HOST + x,
                    catalog.xpath('//a[@class="picture"]/@href').getall())),
            CATEGORY: response.url.split('/')[3],
        })

        cls.main_params(catalog, result)

        return result

    @classmethod
    def main_params(cls, selector, result):
        result.update(dict(
            price_byn=selector.xpath('//div[@class="price-byn"]/text()').get(),
            price_m2=selector.xpath('//div[@class="price-m2"]/node()').get(),
        ))

        cls.get_contacts(selector, result)
        cls.get_structure_house(selector, result)
        cls.get_area(selector, result)
        cls.get_description(selector, result)
        cls.get_dates(selector, result)
        cls.get_analitics(selector, result)
        cls.get_furnishing(selector, result)

    @classmethod
    def get_contacts(cls, selector, result):
        """
        Контакты продавца
        """
        type_agent = selector.xpath('//div[@class="person-type"]/text()').get()

        sub_selector = selector.xpath('//div[@id="person-phones"]')
        if type_agent == 'агент':
            name = sub_selector.xpath('div[@class="company"]/a/text()').get()
        else:
            name = sub_selector.xpath('div[@class="name"]/text()').get()

        phones = sub_selector.xpath('div[@class="phone"]/a/text()').getall()
        phones = ', '.join(phones)
        result.update({
            CONTACT_TYPE: type_agent,
            CONTACT_NAME: name,
            CONTACT_PHONE: phones,
        })

    @classmethod
    def get_structure_house(cls, selector, result):
        """
        Блок "ПАНЕЛЬНЫЙ ДОМ ...."
        """

        result.update({
            ROOM_COUNT: selector.xpath(
                '//div[@class="param-pair-caption" and '
                'text()="Комнат"]/'
                'parent::*/div[@class="param-pair-value"]/text()').get(),
            ROOM_FOR_SALE: selector.xpath(
                '//div[@class="param-pair-caption" and '
                'text()="Комнат на продажу"]/'
                'parent::*/div[@class="param-pair-value"]/text()').get(),
            LEVEL: selector.xpath(
                '//div[@class="param-pair-caption" and '
                'text()="Этаж"]/'
                'parent::*/div[@class="param-pair-value"]/text()').get(),
            CEILING_HEIGHT: selector.xpath(
                '//div[@class="param-pair-caption" and '
                'text()="Высота потолков"]/'
                'parent::*/div[@class="param-pair-value"]/text()').get(),
        })

    @classmethod
    def get_area(cls, selector, result):
        """
        Блок "Площадь"
        """
        area = selector.xpath(
            '//div[@class="param-caption" and text()="Площадь"]/parent::*')

        result.update({
            AREA: area.xpath(
                'div/div[@class="param-pair-caption" and text()="Общая"]/'
                'parent::*/div[@class="param-pair-value"]/text()').get(),
            LAND_AREA: area.xpath(
                'div/div[@class="param-pair-caption" and text()="Участок"]/'
                'parent::*/div[@class="param-pair-value"]/text()').get(),
            LIVING_AREA: area.xpath(
                'div/div[@class="param-pair-caption" and text()="Жилая"]/'
                'parent::*/div[@class="param-pair-value"]/text()').get(),
            KITCHEN_AREA: area.xpath(
                'div/div[@class="param-pair-caption" and text()="Кухня"]/'
                'parent::*/div[@class="param-pair-value"]/text()').get(),
            ROOM_AREA: area.xpath(
                'div/div[@class="param-pair-caption" and text()="Комнаты"]/'
                'parent::*/div[@class="param-pair-value"]/text()').get(),
        })

    @classmethod
    def get_description(cls, selector, result):
        """
        Описание как правило содержат текст в блоке "param-pair-caption",
        но при этом блок "param-pair-value" пустой
        """
        # TODO костыльный способ доставать описание, т.к.
        # блок с описанием никак нельзя четко идентифицировать
        desc = selector.xpath(
            'div/div[@class="main-params"]/div/div[@class="param-pair-value" and '
            'not(text())]/parent::*/div[@class="param-pair-caption"]/text()'
        ).getall()
        if desc:
            desc = desc[-1]

        result.update({
          DESCRIPTION: desc
        })

    @classmethod
    def get_dates(cls, selector, result):
        """
        Блок "Изменения объявления"
        """
        area = selector.xpath(
            '//div[@class="param-caption" '
            'and text()="Изменения объявления"]/parent::*')
        val = area.xpath('div/div[@class="param-pair-value"]/text()').getall()
        result.update({
            CREATED: val[0],
            UPDATED: val[1],
        })

    @classmethod
    def get_analitics(cls, selector, result):
        """
        Блок "Просмотры за последние"
        """
        area = selector.xpath(
            '//div[@class="param-caption" '
            'and text()="Просмотры за последние"]/parent::*')
        val = area.xpath('div/div[@class="param-pair-value"]/text()').getall()
        result.update({
            DAY_VIEW: val[0],
            WEEK_VIEW: val[1],
            MONTH_VIEW: val[2],
        })

    @classmethod
    def get_furnishing(cls, selector, result):
        """
        Блок "ОСНАЩЕНИЕ"
        """
        area = selector.xpath(
            '//div[@class="param-caption" '
            'and text()="Оснащение"]/parent::*')
        val = area.xpath(
            'div/div[@class="param-pair-caption"]/text()').getall()
        result.update({
            FURNISHING: ', '.join(val)
        })
