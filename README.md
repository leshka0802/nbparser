### Build

```
python setup.py sdist --formats gztar --dist-dir ./
```

### Installing

```
virtualenv --python=python3.6 venv
source venv/bin/activate
pip install NBParser-<version>.tar.gz
```

Из архива извлечь файл scrapy.cfg и скопировать в:

linux: домашнюю директорию
    
Windows: c:\\scrapy\\scrapy.cfg

либо в ту директорию, откуда будет запускаться команда

### Usage

```
source /path/to/venv/bin/activate
scrapy crawl NBParser -o result.csv
```
