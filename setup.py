from pip._internal.req import parse_requirements
from pip._internal.download import PipSession
from setuptools import setup, find_packages

REQUIREMENTS = list(map(
    lambda x: str(x.req),
    parse_requirements(
        'requirements.txt', session=PipSession())))

setup(
    name='NBParser',
    version='1.0',
    description='Python Distribution Utilities',
    author='Silantev Aleksey',
    author_email='leska.oo@yandex.ru',
    packages=find_packages(),
    install_requires=REQUIREMENTS,
)